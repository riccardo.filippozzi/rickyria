#include QMK_KEYBOARD_H

// #include "../../rev2/rev2.h"

// QWERTY Home Row Left
#define HR_GUI_A LGUI_T(KC_A)
#define HR_ALT_S LALT_T(KC_S)
#define HR_CTL_D LCTL_T(KC_D)
#define HR_SFT_F LSFT_T(KC_F)

// QWERTY Home Row Right
#define HR_GU_SC RGUI_T(KC_SCLN)
#define HR_ALT_L LALT_T(KC_L)
#define HR_CTL_K RCTL_T(KC_K)
#define HR_SFT_J RSFT_T(KC_J)

// COLEMAK Home Row Left
// #define HR_GUI_A LGUI_T(KC_A)
#define HR_ALT_R LALT_T(KC_R)
#define HR_CTL_S LCTL_T(KC_S)
#define HR_SFT_T LSFT_T(KC_T)

// COLEMAK Home Row Right
#define HR_GUI_O RGUI_T(KC_O)
#define HR_ALT_I LALT_T(KC_I)
#define HR_CTL_E RCTL_T(KC_E)
#define HR_SFT_N RSFT_T(KC_N)


// Shortcuts
#define SH_CTL_Z LCTL(KC_Z)
#define SH_CTL_X LCTL(KC_X)
#define SH_CTL_C LCTL(KC_C)
#define SH_CTL_V LCTL(KC_V)
#define SH_CTL_Y LCTL(KC_Y)
#define SH_CA_TB LCA(KC_TAB)

// Set Default Layer
#define DF_QWERT DF(_QWERTY)
#define DF_CLMAK DF(_COLEMAK)
#define DF_CLKDH DF(_COLEMAK_DH)

// Layers
#define LSYM_BSP LT(_SYMBOLS,KC_BSPC)
#define LMOU_DEL LT(_MOUSE,KC_DEL)
#define LNUM_ENT LT(_NUMERIC,KC_ENT)
#define LNAV_TAB LT(_NAV,KC_TAB)
#define LFUN_ESC LT(_FUNCTIONS,KC_ESC)
#define LADJ_Q   LT(_ADJUST,KC_Q)
#define LADJ_P   LT(_ADJUST,KC_P)
#define LADJ_SCL LT(_ADJUST,KC_SCLN)

// to improve readability
#define __TRNS__ KC_TRNS
#define __HOLD__ KC_TRNS
#define ________ KC_NO


// layers name
enum  {
    _QWERTY,
    _COLEMAK,
    _COLEMAK_DH,
    _NUMERIC,
    _NAV,
    _SYMBOLS,
    _FUNCTIONS,
    _ADJUST,
    _MOUSE
};

// custom keycodes
enum custom_keycodes {
    ASC_GRV = SAFE_RANGE,
    ASC_QUOT,
    ASC_DQUO,
    ASC_TILD,
    ASC_CIRC,
    FD_TIME,
    FD_HUE,
    FD_SAT,
    FD_VAL
};

// limit time to reach fade_rgb.end
#define FADE_TIME_OPTIONS_NUMBER 10
uint8_t fade_time_option_index = 2;
uint16_t fade_time_options[] = { 0, 50, 100, 175, 250, 500, 750, 1000, 1500, 2500};
uint16_t fade_time = 100;
uint16_t fade_timer = 0;

HSV fade_default_hsv = {0, 255, 255};

typedef struct {
    uint16_t cur;     // current RGB value
    uint16_t end;     // RGB value to reach
    uint16_t step;    // RGB value to add/subtract at every step
    // uint16_t speed;   // how often to step (fade_time / fade_rgb.step)
    // uint16_t timer;   // timers
    // uint16_t time;    // time to reach end
} fade_color_t;

typedef struct {
    fade_color_t r;
    fade_color_t g;
    fade_color_t b;
} fade_rgb_t;

fade_rgb_t fade_rgb; // fader: current RGB, end RGB, step RGB
HSV fade_hsv;        // current HSV



const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
[_QWERTY] = LAYOUT(
// ┌────────┬────────┬────────┬────────┬────────┬────────┐                                   ┌────────┬────────┬────────┬────────┬────────┬────────┐
    ________,LADJ_Q,  KC_W    ,KC_E    ,KC_R    ,KC_T    ,                                    KC_Y    ,KC_U    ,KC_I    ,KC_O    ,LADJ_P  ,________,
// ├────────┼────────┼────────┼────────┼────────┼────────┤              QWERTY               ├────────┼────────┼────────┼────────┼────────┼────────┤
    ________,HR_GUI_A,HR_ALT_S,HR_CTL_D,HR_SFT_F,KC_G    ,                                    KC_H    ,HR_SFT_J,HR_CTL_K,HR_ALT_L,HR_GU_SC,________,
// ├────────┼────────┼────────┼────────┼────────┼────────┼────────┬────────┬────────┬────────┼────────┼────────┼────────┼────────┼────────┼────────┤
    ________,KC_Z    ,KC_X    ,KC_C    ,KC_V    ,KC_B    ,________,________,________,________,KC_N    ,KC_M    ,KC_COMM ,KC_DOT  ,KC_SLSH ,________,
// └────────┴────────┴────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┴────────┴────────┘
                               ________,LFUN_ESC,KC_SPC  ,LNAV_TAB,________,________,LNUM_ENT,LSYM_BSP,LMOU_DEL,________
//                            └────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┘
),
[_NAV] = LAYOUT(
// ┌────────┬────────┬────────┬────────┬────────┬────────┐                                   ┌────────┬────────┬────────┬────────┬────────┬────────┐
    ________,SH_CTL_Y,KC_HOME ,KC_UP   ,KC_END  ,KC_PGUP ,                                    ________,KC_CAPS ,KC_NUM  ,KC_INS  ,________,________,
// ├────────┼────────┼────────┼────────┼────────┼────────┤             NAVIGATION            ├────────┼────────┼────────┼────────┼────────┼────────┤
    ________,SH_CA_TB,KC_LEFT ,KC_DOWN ,KC_RGHT ,KC_PGDN ,                                    KC_APP  ,KC_LSFT ,KC_LCTL ,KC_LALT ,KC_LGUI ,________,
// ├────────┼────────┼────────┼────────┼────────┼────────┼────────┬────────┬────────┬────────┼────────┼────────┼────────┼────────┼────────┼────────┤
    ________,SH_CTL_Z,SH_CTL_X,SH_CTL_C,SH_CTL_V,________,________,________,________,________,________,________,________,________,________,________,
// └────────┴────────┴────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┴────────┴────────┘
                               ________,__TRNS__,__TRNS__,__HOLD__,________,________,__TRNS__,__TRNS__,__TRNS__,________
//                            └────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┘
),
[_MOUSE] = LAYOUT(
// ┌────────┬────────┬────────┬────────┬────────┬────────┐                                   ┌────────┬────────┬────────┬────────┬────────┬────────┐
    ________,SH_CTL_Y,KC_BTN2 ,KC_MS_U ,KC_BTN1 ,KC_WH_U ,                                    KC_BTN3 ,KC_BTN1 ,KC_WH_U ,KC_BTN2 , KC_SCRL,________,
// ├────────┼────────┼────────┼────────┼────────┼────────┤               MOUSE               ├────────┼────────┼────────┼────────┼────────┼────────┤
    ________,KC_PSCR ,KC_MS_L ,KC_MS_D ,KC_MS_R ,KC_WH_D ,                                    KC_BTN4 ,KC_WH_L ,KC_WH_D ,KC_WH_R ,________,________,
// ├────────┼────────┼────────┼────────┼────────┼────────┼────────┬────────┬────────┬────────┼────────┼────────┼────────┼────────┼────────┼────────┤
    ________,SH_CTL_Z,SH_CTL_X,SH_CTL_C,SH_CTL_V,________,________,________,________,________,KC_BTN5 ,KC_ACL0 ,KC_ACL1 ,KC_ACL2 ,________,________,
// └────────┴────────┴────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┴────────┴────────┘
                               ________,__TRNS__,KC_BTN2 ,KC_BTN1 ,________,________,__TRNS__,__TRNS__,__HOLD__,________
//                            └────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┘
),
[_SYMBOLS] = LAYOUT(
// ┌────────┬────────┬────────┬────────┬────────┬────────┐                                   ┌────────┬────────┬────────┬────────┬────────┬────────┐
    ________,________,ASC_GRV ,ASC_QUOT,ASC_DQUO,________,                                    ________,KC_LCBR ,KC_RCBR ,KC_AT   ,________,________,
// ├────────┼────────┼────────┼────────┼────────┼────────┤              SYMBOLS              ├────────┼────────┼────────┼────────┼────────┼────────┤
    ________,ASC_TILD,KC_AMPR ,KC_EXLM ,KC_PIPE ,KC_PERC ,                                    KC_DLR  ,KC_LPRN ,KC_RPRN ,KC_UNDS ,KC_HASH ,________,
// ├────────┼────────┼────────┼────────┼────────┼────────┼────────┬────────┬────────┬────────┼────────┼────────┼────────┼────────┼────────┼────────┤
    ________,ASC_CIRC,KC_LT   ,KC_GT   ,KC_EQL  ,________,________,________,________,________,________,KC_LBRC ,KC_RBRC ,________,________,________,
// └────────┴────────┴────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┴────────┴────────┘
                               ________,__TRNS__,__TRNS__,__TRNS__,________,________,__TRNS__,__HOLD__,__TRNS__,________
//                            └────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┘
),
[_NUMERIC] = LAYOUT(
// ┌────────┬────────┬────────┬────────┬────────┬────────┐                                   ┌────────┬────────┬────────┬────────┬────────┬────────┐
    ________,________,KC_GRV  ,KC_QUOT ,KC_DQUO ,________,                                    ________,KC_7    ,KC_8    ,KC_9    ,KC_COMM ,________,
// ├────────┼────────┼────────┼────────┼────────┼────────┤              NUMERIC              ├────────┼────────┼────────┼────────┼────────┼────────┤
    ________,KC_SLSH ,KC_ASTR ,KC_MINS ,KC_PLUS ,KC_PERC ,                                    KC_0    ,KC_4    ,KC_5    ,KC_6    ,KC_EQL  ,________,
// ├────────┼────────┼────────┼────────┼────────┼────────┼────────┬────────┬────────┬────────┼────────┼────────┼────────┼────────┼────────┼────────┤
    ________,KC_BSLS ,KC_LT   ,KC_GT   ,KC_EQL  ,________,________,________,________,________,KC_0    ,KC_1    ,KC_2    ,KC_3    ,KC_DOT  ,________,
// └────────┴────────┴────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┴────────┴────────┘
                               ________,__TRNS__,__TRNS__,__TRNS__,________,________,__HOLD__,__TRNS__,__TRNS__,________
//                            └────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┘
),
[_FUNCTIONS] = LAYOUT(
// ┌────────┬────────┬────────┬────────┬────────┬────────┐                                   ┌────────┬────────┬────────┬────────┬────────┬────────┐
    ________,________,KC_MRWD ,KC_PAUS ,KC_MFFD ,KC_VOLU ,                                    ________,KC_F7   ,KC_F8   ,KC_F9   ,KC_F10  ,________,
// ├────────┼────────┼────────┼────────┼────────┼────────┤             FUNCTIONS             ├────────┼────────┼────────┼────────┼────────┼────────┤
    ________,KC_LGUI ,KC_LALT ,KC_LCTL ,KC_LSFT ,KC_VOLD ,                                    ________,KC_F4   ,KC_F5   ,KC_F6   ,KC_F11  ,________,
// ├────────┼────────┼────────┼────────┼────────┼────────┼────────┬────────┬────────┬────────┼────────┼────────┼────────┼────────┼────────┼────────┤
    ________,________,KC_MPRV ,KC_MPLY ,KC_MNXT ,KC_MUTE ,________,________,________,________,________,KC_F1   ,KC_F2   ,KC_F3   ,KC_F12  ,________,
// └────────┴────────┴────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┴────────┴────────┘
                               ________,__HOLD__,__TRNS__,__TRNS__,________,________,__TRNS__,__TRNS__,__TRNS__,________
//                            └────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┘
),
[_ADJUST] = LAYOUT(
// ┌────────┬────────┬────────┬────────┬────────┬────────┐                                   ┌────────┬────────┬────────┬────────┬────────┬────────┐
    ________,__HOLD__,DF_CLKDH,DF_CLMAK,DF_QWERT,________,                                    ________,________,________,________,__HOLD__,________,
// ├────────┼────────┼────────┼────────┼────────┼────────┤              ADJUST               ├────────┼────────┼────────┼────────┼────────┼────────┤
    ________,FD_TIME ,FD_HUE  ,FD_SAT  ,FD_VAL  ,________,                                    RGB_SPI ,RGB_HUI ,RGB_SAI ,RGB_VAI ,RGB_MOD ,________,
// ├────────┼────────┼────────┼────────┼────────┼────────┼────────┬────────┬────────┬────────┼────────┼────────┼────────┼────────┼────────┼────────┤
    ________,________,________,________,________,________,________,________,________,________,RGB_SPD ,RGB_HUD ,RGB_SAD ,RGB_VAD ,RGB_RMOD,________,
// └────────┴────────┴────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┴────────┴────────┘
                               ________,QK_BOOT ,__TRNS__,__TRNS__,________,________,RGB_TOG ,__TRNS__,QK_BOOT ,________
//                            └────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┘
),
[_COLEMAK] = LAYOUT(
// ┌────────┬────────┬────────┬────────┬────────┬────────┐                                   ┌────────┬────────┬────────┬────────┬────────┬────────┐
    ________,LADJ_Q  ,KC_W    ,KC_F    ,KC_P    ,KC_G    ,                                    KC_J    ,KC_L    ,KC_U    ,KC_Y    ,LADJ_SCL,________,
// ├────────┼────────┼────────┼────────┼────────┼────────┤              COLEMAK              ├────────┼────────┼────────┼────────┼────────┼────────┤
    ________,HR_GUI_A,HR_ALT_R,HR_CTL_S,HR_SFT_T,KC_D    ,                                    KC_H    ,HR_SFT_N,HR_CTL_E,HR_ALT_I,HR_GUI_O,________,
// ├────────┼────────┼────────┼────────┼────────┼────────┼────────┬────────┬────────┬────────┼────────┼────────┼────────┼────────┼────────┼────────┤
    ________,KC_Z    ,KC_X    ,KC_C    ,KC_V    ,KC_B    ,________,________,________,________,KC_K    ,KC_M    ,KC_COMM ,KC_DOT  ,KC_SLSH ,________,
// └────────┴────────┴────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┴────────┴────────┘
                               ________,LFUN_ESC,KC_SPC  ,LNAV_TAB,________,________,LNUM_ENT,LSYM_BSP,LMOU_DEL,________
//                            └────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┘
),
[_COLEMAK_DH] = LAYOUT(
// ┌────────┬────────┬────────┬────────┬────────┬────────┐                                   ┌────────┬────────┬────────┬────────┬────────┬────────┐
    ________,LADJ_Q,  KC_W    ,KC_F    ,KC_P    ,KC_B    ,                                    KC_J    ,KC_L    ,KC_U    ,KC_Y    ,LADJ_SCL,________,
// ├────────┼────────┼────────┼────────┼────────┼────────┤             COLEMAK DH            ├────────┼────────┼────────┼────────┼────────┼────────┤
    ________,HR_GUI_A,HR_ALT_R,HR_CTL_S,HR_SFT_T,KC_G    ,                                    KC_M    ,HR_SFT_N,HR_CTL_E,HR_ALT_I,HR_GUI_O,________,
// ├────────┼────────┼────────┼────────┼────────┼────────┼────────┬────────┬────────┬────────┼────────┼────────┼────────┼────────┼────────┼────────┤
    ________,KC_Z    ,KC_X    ,KC_C    ,KC_D    ,KC_V    ,________,________,________,________,KC_K    ,KC_H    ,KC_COMM ,KC_DOT  ,KC_SLSH ,________,
// └────────┴────────┴────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┴────────┴────────┘
                               ________,LFUN_ESC,KC_SPC  ,LNAV_TAB,________,________,LNUM_ENT,LSYM_BSP,LMOU_DEL,________
//                            └────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┘
),
};

// ┌────────┬────────┬────────┬────────┬────────┬────────┐                                   ┌────────┬────────┬────────┬────────┬────────┬────────┐
//  ________,__TRNS__,__TRNS__,__TRNS__,__TRNS__,__TRNS__,                                    __TRNS__,__TRNS__,__TRNS__,__TRNS__,__TRNS__,________,
// ├────────┼────────┼────────┼────────┼────────┼────────┤                                   ├────────┼────────┼────────┼────────┼────────┼────────┤
//  ________,__TRNS__,__TRNS__,__TRNS__,__TRNS__,__TRNS__,                                    __TRNS__,__TRNS__,__TRNS__,__TRNS__,__TRNS__,________,
// ├────────┼────────┼────────┼────────┼────────┼────────┼────────┬────────┬────────┬────────┼────────┼────────┼────────┼────────┼────────┼────────┤
//  ________,__TRNS__,__TRNS__,__TRNS__,__TRNS__,__TRNS__,________,________,________,________,__TRNS__,__TRNS__,__TRNS__,__TRNS__,__TRNS__,________,
// └────────┴────────┴────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┴────────┴────────┘
//                             ________,________,__TRNS__,__TRNS__,__TRNS__,__TRNS__,__TRNS__,__TRNS__,________,________
//                            └────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┴────────┘


bool process_record_user(uint16_t keycode, keyrecord_t *record){
    // process custom keycodes
    if (record->event.pressed) {
        switch(keycode) {
            case ASC_GRV:
                SEND_STRING("` ");
            	return false;
            case ASC_QUOT:
                SEND_STRING("' ");
            	return false;
            case ASC_DQUO:
                SEND_STRING("\" ");
                return false;
            case ASC_TILD:
                SEND_STRING("~ ");
                return false;
            case ASC_CIRC:
                SEND_STRING("^ ");
                return false;

            case FD_TIME:
                if(++fade_time_option_index>=FADE_TIME_OPTIONS_NUMBER)
                    fade_time_option_index = 0;
                fade_time = fade_time_options[fade_time_option_index];
                return false;
            case FD_HUE:
                // fade_default_hsv.h+=16;
                // if(fade_default_hsv.h == 0)
                //     fade_default_hsv.h = 255;
                // else if(fade_default_hsv.h == 15)
                //         fade_default_hsv.h = 0;
                return false;
            case FD_SAT:
                fade_default_hsv.s+=16;
                if(fade_default_hsv.s == 0)
                    fade_default_hsv.s = 255;
                else if(fade_default_hsv.s == 15)
                        fade_default_hsv.s = 0;
                return false;
            case FD_VAL:
                fade_default_hsv.v+=16;
                if(fade_default_hsv.v == 0)
                    fade_default_hsv.v = 255;
                else if(fade_default_hsv.v == 15)
                        fade_default_hsv.v = 0;
                return false;
        }
    }
    return true;
};



#ifdef RGBLIGHT_ENABLE
/*
const uint8_t quadratic_curve[256] = {
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   1,   1,   1,   1,
  1,   1,   1,   1,   2,   2,   2,   2,   2,   2,   3,   3,   3,   3,   4,   4,
  4,   4,   5,   5,   5,   5,   6,   6,   6,   7,   7,   7,   8,   8,   8,   9,
  9,   9,  10,  10,  11,  11,  11,  12,  12,  13,  13,  14,  14,  15,  15,  16,
 16,  17,  17,  18,  18,  19,  19,  20,  20,  21,  21,  22,  23,  23,  24,  24,
 25,  26,  26,  27,  28,  28,  29,  30,  30,  31,  32,  32,  33,  34,  35,  35,
 36,  37,  38,  38,  39,  40,  41,  42,  42,  43,  44,  45,  46,  47,  47,  48,
 49,  50,  51,  52,  53,  54,  55,  55,  56,  57,  58,  59,  60,  61,  62,  63,
 64,  65,  66,  67,  68,  69,  70,  71,  72,  74,  75,  76,  77,  78,  79,  80,
 81,  82,  84,  85,  86,  87,  88,  89,  91,  92,  93,  94,  95,  97,  98,  99,
100, 102, 103, 104, 105, 107, 108, 109, 111, 112, 113, 115, 116, 117, 119, 120,
121, 123, 124, 126, 127, 128, 130, 131, 133, 134, 136, 137, 139, 140, 141, 143,
144, 146, 147, 149, 151, 152, 154, 155, 157, 158, 160, 161, 163, 165, 166, 168,
170, 171, 173, 174, 176, 178, 179, 181, 183, 185, 186, 188, 190, 191, 193, 195,
197, 198, 200, 202, 204, 206, 207, 209, 211, 213, 215, 216, 218, 220, 222, 224,
226, 228, 229, 231, 233, 235, 237, 239, 241, 243, 245, 247, 249, 251, 253, 255
};

const uint8_t exp_curve[256] =
{
   0,   0,   0,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,
   1,   1,   1,   2,   2,   2,   2,   2,   2,   2,   2,   2,   2,   2,   2,   2,
   2,   2,   2,   2,   2,   2,   2,   2,   2,   2,   2,   3,   3,   3,   3,   3,
   3,   3,   3,   3,   3,   3,   3,   3,   3,   3,   4,   4,   4,   4,   4,   4,
   4,   4,   4,   4,   4,   4,   5,   5,   5,   5,   5,   5,   5,   5,   5,   6,
   6,   6,   6,   6,   6,   6,   6,   7,   7,   7,   7,   7,   7,   8,   8,   8,
   8,   8,   8,   9,   9,   9,   9,   9,  10,  10,  10,  10,  10,  11,  11,  11,
  11,  12,  12,  12,  12,  13,  13,  13,  14,  14,  14,  14,  15,  15,  15,  16,
  16,  16,  17,  17,  18,  18,  18,  19,  19,  20,  20,  21,  21,  21,  22,  22,
  23,  23,  24,  24,  25,  25,  26,  27,  27,  28,  28,  29,  30,  30,  31,  32,
  32,  33,  34,  35,  35,  36,  37,  38,  39,  39,  40,  41,  42,  43,  44,  45,
  46,  47,  48,  49,  50,  51,  52,  53,  55,  56,  57,  58,  59,  61,  62,  63,
  65,  66,  68,  69,  71,  72,  74,  76,  77,  79,  81,  82,  84,  86,  88,  90,
  92,  94,  96,  98, 100, 102, 105, 107, 109, 112, 114, 117, 119, 122, 124, 127,
 130, 133, 136, 139, 142, 145, 148, 151, 155, 158, 162, 165, 169, 172, 176, 180,
 184, 188, 192, 196, 201, 205, 210, 214, 219, 224, 229, 234, 239, 244, 250, 255
};
*/

void rgb_to_hsv(fade_rgb_t *rgb, HSV *hsv){
    int32_t min, max, delta, hue =0;
    int32_t R = rgb->r.cur>>8;
    int32_t G = rgb->g.cur>>8;
    int32_t B = rgb->b.cur>>8;

    // find min
    min = (R < G ? R : G);
    min = (min < B ? min : B);
    // find max
    max = (R > G ? R : G);
    max = (max > B ? max : B);
    // calculate delta
    delta = max - min;

    // calculate HUE
    if(R == max)
        hue = ((G - B)*60/delta);

    else if(G == max)
        hue = (120 + ((B - R)*60/delta));

    else if(B == max)
        hue = (240 + ((R - G)*60/delta));

    if(hue < 0)
        hue += 360;
    hsv->h = hue*255/360;

    // calculate Saturation
    hsv->s = 0;
    if( max != 0 )
        hsv->s = ((delta*255) / max);
    // check max saturation
    if( hsv->s > fade_default_hsv.s )
        hsv->s = fade_default_hsv.s;

    // set Value
    hsv->v = max * fade_default_hsv.v / 255;
    // hsv->v = quadratic_curve[hsv->v];
    // hsv->v = exp_curve[hsv->v];
}

void fade_color_scan(fade_color_t *c){
    if(fade_timer < fade_time ){
        // do step
        if(c->cur < c->end){
            if(c->cur >= c->end-c->step){
                c->cur = c->end;
            }else{
                c->cur += c->step;
            }
        }else{
            // cur > end
            if(c->cur <= c->end+c->step){
                c->cur = c->end;
            }else{
                c->cur -= c->step;
            }
        }
    }else{
        c->cur = c->end;
    }
}

void fade_rgb_scan(void){
    // increment fade_timer
    if(fade_timer < fade_time)
        fade_timer++;

    // fade current to end
    fade_color_scan(&fade_rgb.r);
    fade_color_scan(&fade_rgb.g);
    fade_color_scan(&fade_rgb.b);

    // set hsv
    rgb_to_hsv(&fade_rgb, &fade_hsv);
    rgblight_sethsv_noeeprom(fade_hsv.h, fade_hsv.s, fade_hsv.v);
}

#define FADE_CALC_STEP(cur, end, ft) (((cur>end ? cur-end : end-cur)/ft)+1)

void fade_goto(uint8_t end_r, uint8_t end_g, uint8_t end_b){

    if( (fade_rgb.r.end>>8 == end_r) &&
        (fade_rgb.g.end>>8 == end_g) &&
        (fade_rgb.b.end>>8 == end_b) ) return;

    fade_rgb.r.end = end_r<<8;
    fade_rgb.g.end = end_g<<8;
    fade_rgb.b.end = end_b<<8;

    fade_rgb.r.step = FADE_CALC_STEP(fade_rgb.r.cur, fade_rgb.r.end, fade_time);
    fade_rgb.g.step = FADE_CALC_STEP(fade_rgb.g.cur, fade_rgb.g.end, fade_time);
    fade_rgb.b.step = FADE_CALC_STEP(fade_rgb.b.cur, fade_rgb.b.end, fade_time);

    fade_timer = 0;
}

void fade_init(void){
    rgblight_sethsv_noeeprom(0, 0, 0); // set leds off
    fade_time = fade_time_options[fade_time_option_index];
}

//         Led Scheme
//    left             right
// 9 2 1 0     |       10 11 12 19
// 8 3 4 5 6 7 | 17 16 15 14 13 18

void set_current_layer_color_leds(layer_state_t state){
    // Keyboard Layer Status
    switch (get_highest_layer(state)) {
        case _NUMERIC:
            fade_goto(RGB_RED);
            break;
        case _NAV:
            fade_goto(RGB_BLUE);
            break;
        case _SYMBOLS:
            fade_goto(RGB_GREEN);
            break;
        case _FUNCTIONS:
            fade_goto(RGB_YELLOW);
            break;
        case _ADJUST:
            fade_goto(RGB_PURPLE);
            break;
        case _MOUSE:
            fade_goto(RGB_CYAN);
            break;
        default:
            fade_goto(RGB_WHITE);
    }
}

layer_state_t layer_state_set_user(layer_state_t state) {
    set_current_layer_color_leds(state);
    return state;
}

#endif

void matrix_scan_user(){ // called every 1ms
    fade_rgb_scan();
}

void keyboard_pre_init_user(){
    fade_init();
    fade_goto(RGB_WHITE);
}


#ifdef OLED_ENABLE

void oled_write_header(void){
    // QMK Logo and version information
    // clang-format off
    static const char PROGMEM qmk_logo[] = {
        0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8a,0x8b,0x8c,0x8d,0x8e,0x8f,0x90,0x91,0x92,0x93,0x94,
        0xa0,0xa1,0xa2,0xa3,0xa4,0xa5,0xa6,0xa7,0xa8,0xa9,0xaa,0xab,0xac,0xad,0xae,0xaf,0xb0,0xb1,0xb2,0xb3,0xb4,
        0xc0,0xc1,0xc2,0xc3,0xc4,0xc5,0xc6,0xc7,0xc8,0xc9,0xca,0xcb,0xcc,0xcd,0xce,0xcf,0xd0,0xd1,0xd2,0xd3,0xd4,0};
    // clang-format on
    oled_write_P(qmk_logo, false);

    oled_write_P(PSTR("RicKyria "), false);
#if defined(KEYBOARD_splitkb_kyria_rev1)
    oled_write_P(PSTR("rev1"), false);
#elif defined(KEYBOARD_splitkb_kyria_rev2)
    oled_write_P(PSTR("rev2"), false);
#endif
}

void oled_write_layers(void){
    // Host Keyboard Layer Status
    // oled_write_P(PSTR("\nLayer: "), false);
    oled_write_P(PSTR("\nLayer: "), false);
    switch (get_highest_layer(layer_state | default_layer_state)) {
        case _QWERTY:
            oled_write_P(PSTR("QWERTY\n"), false);
            break;
        case _COLEMAK:
            oled_write_P(PSTR("Colemak\n"), false);
            break;
        case _COLEMAK_DH:
            oled_write_P(PSTR("Colemak DH\n"), false);
            break;
        case _NUMERIC:
            oled_write_P(PSTR("Numeric\n"), false);
            break;
        case _NAV:
            oled_write_P(PSTR("Navigation\n"), false);
            break;
        case _SYMBOLS:
            oled_write_P(PSTR("Symbols\n"), false);
            break;
        case _FUNCTIONS:
            oled_write_P(PSTR("Functions\n"), false);
            break;
        case _ADJUST:
            oled_write_P(PSTR("Adjust\n"), false);
            break;
        case _MOUSE:
            oled_write_P(PSTR("Mouse\n"), false);
            break;
        default:break;
            // oled_write_P(PSTR("What?\n"), false);
    }
}

void oled_write_indicators(void){
    // Host Keyboard LED Status
    led_t led_usb_state = host_keyboard_led_state();
    oled_write_P(led_usb_state.num_lock ? PSTR("NUMLCK ") : PSTR("       "), false);
    oled_write_P(led_usb_state.caps_lock ? PSTR("CAPLCK ") : PSTR("       "), false);
    oled_write_P(led_usb_state.scroll_lock ? PSTR("SCRLCK ") : PSTR("       "), false);
}

// void oled_write_kyria_logo(void){
//     // clang-format off
//     static const char PROGMEM kyria_logo[] = {
//         0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,128,128,192,224,240,112,120, 56, 60, 28, 30, 14, 14, 14,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7, 14, 14, 14, 30, 28, 60, 56,120,112,240,224,192,128,128,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
//         0,  0,  0,  0,  0,  0,  0,192,224,240,124, 62, 31, 15,  7,  3,  1,128,192,224,240,120, 56, 60, 28, 30, 14, 14,  7,  7,135,231,127, 31,255,255, 31,127,231,135,  7,  7, 14, 14, 30, 28, 60, 56,120,240,224,192,128,  1,  3,  7, 15, 31, 62,124,240,224,192,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
//         0,  0,  0,  0,240,252,255, 31,  7,  1,  0,  0,192,240,252,254,255,247,243,177,176, 48, 48, 48, 48, 48, 48, 48,120,254,135,  1,  0,  0,255,255,  0,  0,  1,135,254,120, 48, 48, 48, 48, 48, 48, 48,176,177,243,247,255,254,252,240,192,  0,  0,  1,  7, 31,255,252,240,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
//         0,  0,  0,255,255,255,  0,  0,  0,  0,  0,254,255,255,  1,  1,  7, 30,120,225,129,131,131,134,134,140,140,152,152,177,183,254,248,224,255,255,224,248,254,183,177,152,152,140,140,134,134,131,131,129,225,120, 30,  7,  1,  1,255,255,254,  0,  0,  0,  0,  0,255,255,255,  0,  0,  0,  0,255,255,  0,  0,192,192, 48, 48,  0,  0,240,240,  0,  0,  0,  0,  0,  0,240,240,  0,  0,240,240,192,192, 48, 48, 48, 48,192,192,  0,  0, 48, 48,243,243,  0,  0,  0,  0,  0,  0, 48, 48, 48, 48, 48, 48,192,192,  0,  0,  0,  0,  0,
//         0,  0,  0,255,255,255,  0,  0,  0,  0,  0,127,255,255,128,128,224,120, 30,135,129,193,193, 97, 97, 49, 49, 25, 25,141,237,127, 31,  7,255,255,  7, 31,127,237,141, 25, 25, 49, 49, 97, 97,193,193,129,135, 30,120,224,128,128,255,255,127,  0,  0,  0,  0,  0,255,255,255,  0,  0,  0,  0, 63, 63,  3,  3, 12, 12, 48, 48,  0,  0,  0,  0, 51, 51, 51, 51, 51, 51, 15, 15,  0,  0, 63, 63,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 48, 48, 63, 63, 48, 48,  0,  0, 12, 12, 51, 51, 51, 51, 51, 51, 63, 63,  0,  0,  0,  0,  0,
//         0,  0,  0,  0, 15, 63,255,248,224,128,  0,  0,  3, 15, 63,127,255,239,207,141, 13, 12, 12, 12, 12, 12, 12, 12, 30,127,225,128,  0,  0,255,255,  0,  0,128,225,127, 30, 12, 12, 12, 12, 12, 12, 12, 13,141,207,239,255,127, 63, 15,  3,  0,  0,128,224,248,255, 63, 15,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
//         0,  0,  0,  0,  0,  0,  0,  3,  7, 15, 62,124,248,240,224,192,128,  1,  3,  7, 15, 30, 28, 60, 56,120,112,112,224,224,225,231,254,248,255,255,248,254,231,225,224,224,112,112,120, 56, 60, 28, 30, 15,  7,  3,  1,128,192,224,240,248,124, 62, 15,  7,  3,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
//         0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  3,  7, 15, 14, 30, 28, 60, 56,120,112,112,112,224,224,224,224,224,224,224,224,224,224,224,224,224,224,224,224,112,112,112,120, 56, 60, 28, 30, 14, 15,  7,  3,  1,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
//     };
//     // clang-format on
//     oled_write_raw_P(kyria_logo, sizeof(kyria_logo));
// }

// void oled_write_debug_smart_layers(void){
//     oled_write(get_u8_str((layer_lock>>4)&LAYER_LOCK_LEFT), false);
//     oled_write(get_u8_str(layer_lock&LAYER_LOCK_LEFT), false);
//     oled_write(get_u8_str(((layer_lock>>4)&LAYER_LOCK_RIGHT)>>1), false);
//     oled_write(get_u8_str((layer_lock&LAYER_LOCK_RIGHT)>>1), false);
// }

// void oled_write_wpm(void){
// #ifdef WPM_ENABLE
//     // print WPM
//     oled_write_P(PSTR("\nWPM: "), false);
//     oled_write(get_u8_str(get_current_wpm(), ' '), false);
// #endif
// }

void oled_write_fader_values(void){

    oled_write_P(PSTR("     Fader Values"), false);
    // oled_write_P(PSTR("Fader RGB Values"), false);

    oled_write_P(PSTR("\ne:"), false);
    oled_write(get_u16_str(fade_rgb.r.end, ' '), false);
    oled_write_P(PSTR(" "), false);
    oled_write(get_u16_str(fade_rgb.g.end, ' '), false);
    oled_write_P(PSTR(" "), false);
    oled_write(get_u16_str(fade_rgb.b.end, ' '), false);

    oled_write_P(PSTR("\nc:"), false);
    oled_write(get_u16_str(fade_rgb.r.cur, ' '), false);
    oled_write_P(PSTR(" "), false);
    oled_write(get_u16_str(fade_rgb.g.cur, ' '), false);
    oled_write_P(PSTR(" "), false);
    oled_write(get_u16_str(fade_rgb.b.cur, ' '), false);

    oled_write_P(PSTR("\ns:"), false);
    oled_write(get_u16_str(fade_rgb.r.step, ' '), false);
    oled_write_P(PSTR(" "), false);
    oled_write(get_u16_str(fade_rgb.g.step, ' '), false);
    oled_write_P(PSTR(" "), false);
    oled_write(get_u16_str(fade_rgb.b.step, ' '), false);

    oled_write_P(PSTR("\nhsv:"), false);
    oled_write(get_u8_str(fade_hsv.h, ' '), false);
    oled_write_P(PSTR(" "), false);
    oled_write(get_u8_str(fade_hsv.s, ' '), false);
    oled_write_P(PSTR(" "), false);
    oled_write(get_u8_str(fade_hsv.v, ' '), false);
    oled_write_P(PSTR(" "), false);

    oled_write_P(PSTR("\ndef:"), false);
    oled_write(get_u8_str(fade_default_hsv.h, ' '), false);
    oled_write_P(PSTR(" "), false);
    oled_write(get_u8_str(fade_default_hsv.s, ' '), false);
    oled_write_P(PSTR(" "), false);
    oled_write(get_u8_str(fade_default_hsv.v, ' '), false);
    oled_write_P(PSTR(" "), false);

    oled_write_P(PSTR("\nT:"), false);
    oled_write(get_u16_str(fade_time, ' '), false);
    oled_write_P(PSTR(" t:"), false);
    oled_write(get_u16_str(fade_timer, ' '), false);

}

oled_rotation_t oled_init_user(oled_rotation_t rotation) {
    return OLED_ROTATION_180;
}

bool oled_task_user(void) {
    if (is_keyboard_master()) {
        oled_write_fader_values();
    } else {

        oled_write_header();
        oled_write_layers();
        oled_write_indicators();
        // olde_write_kyria_logo();

    }
    return false;
}

#endif
