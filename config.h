/* Copyright 2022 Riccardo Filippozzi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#define SPLIT_HAND_MATRIX_GRID GP7, GP20 // row first because the board is col2row
#define SPLIT_HAND_MATRIX_GRID_LOW_IS_RIGHT
#define MATRIX_MASKED // actual mask is defined by `matrix_mask` in `rev2.c`

// I2C driver
#define I2C_DRIVER	    I2CD1
#define I2C1_SDA_PIN    GP2
#define I2C1_SCL_PIN	GP3
#define OLED_DISPLAY_ADDRESS 0x3C

// # SPI driver
// #define SPI_DRIVER	    SPID0
// #define SPI_SCK_PIN	    GP18
// #define SPI_MISO_PIN	GP20
// #define SPI_MOSI_PIN	GP19

// Serial driver
// #define SERIAL_USART_DRIVER	SIOD0
// #define SERIAL_USART_TX_PIN	GP0
// #define SERIAL_USART_RX_PIN	GP1

// #undef SOFT_SERIAL_PIN
// #define SOFT_SERIAL_PIN GP1

// #undef RGB_DI_PIN
// #define RGB_DI_PIN GP0

// forse serve
// #define WS2812_TRST_US 80
#define WS2812_PIO_USE_PIO1 // Force the usage of PIO1 peripheral, by default the WS2812 implementation uses the PIO0 peripheral

#ifdef RGBLIGHT_ENABLE
// #    define RGBLIGHT_ANIMATIONS
#    define RGBLIGHT_HUE_STEP  8
#    define RGBLIGHT_SAT_STEP  8
#    define RGBLIGHT_VAL_STEP  8
#    define RGBLIGHT_LIMIT_VAL 150
#    define RGBLIGHT_SLEEP
#    define RGBLIGHT_SPLIT
#    define RGBLIGHT_LAYERS
#endif

/* RGB matrix support */
#ifdef RGB_MATRIX_ENABLE
    #ifndef SPLIT_LAYER_STATE_ENABLE
        #define SPLIT_LAYER_STATE_ENABLE
    #endif
#endif

// Lets you roll mod-tap keys
// #define IGNORE_MOD_TAP_INTERRUPT
#define TAPPING_TERM 200

// Set USB polling rate to 1000 Hz
#define USB_POLLING_INTERVAL_MS 1

#ifdef OLED_ENABLE
    #define SPLIT_LED_STATE_ENABLE
    #ifndef SPLIT_LAYER_STATE_ENABLE
        #define SPLIT_LAYER_STATE_ENABLE
    #endif
#endif

// #ifdef RGBLIGHT_SPLIT
// #undef RGBLIGHT_SPLIT
// #endif

// #define SPLIT_WATCHDOG_ENABLE

#ifdef OLED_ENABLE
#    define OLED_DISPLAY_128X64
#    define SPLIT_OLED_ENABLE
#endif

/* RGB matrix support */
#ifdef RGB_MATRIX_ENABLE
#    define SPLIT_TRANSPORT_MIRROR
#    define RGB_MATRIX_LED_COUNT 20 // Number of LEDs
#    define RGB_MATRIX_SPLIT { 10, 10 }
#    define RGB_MATRIX_MAXIMUM_BRIGHTNESS 170
#    define RGB_DISABLE_WHEN_USB_SUSPENDED
#endif
