

rules.mk
``` makefile
# MCU name
MCU = RP2040
# BOARD = GENERIC_RP_RP2040
# Bootloader selection
BOOTLOADER = rp2040

SERIAL_DRIVER = vendor
WS2812_DRIVER = vendor

OLED_ENABLE = yes
OLED_DRIVER = SSD1306   # Enables the use of OLED displays

RGB_MATRIX_DRIVER = WS2812

# DEFAULT_FOLDER = splitkb/kyria/rev2

# OPT_DEFS += -DRP2040_MCUCONF
# OPT_DEFS += -DRP_I2C_USE_I2C0=TRUE
# OPT_DEFS += -DRP_I2C_USE_I2C1=TRUE
# OPT_DEFS += -DHAL_USE_I2C=TRUE
# OPT_DEFS += -DPAL_USE_CALLBACKS

```

config.h
``` C
// I2C driver
#define I2C_DRIVER	    I2CD2
#define I2C1_SDA_PIN    GP2
#define I2C1_SCL_PIN	GP3
#define OLED_DISPLAY_ADDRESS 0x3C

// # SPI driver
// #define SPI_DRIVER	    SPID0
// #define SPI_SCK_PIN	    GP18
// #define SPI_MISO_PIN	GP20
// #define SPI_MOSI_PIN	GP19

// Serial driver
#define SERIAL_USART_DRIVER	SIOD0
// #define SERIAL_USART_TX_PIN	GP0
#define SERIAL_USART_RX_PIN	GP1

#undef SOFT_SERIAL_PIN
#define SOFT_SERIAL_PIN GP1

#undef RGB_DI_PIN
#define RGB_DI_PIN GP0

// forse serve
// #define WS2812_TRST_US 80
#define WS2812_PIO_USE_PIO1 // Force the usage of PIO1 peripheral, by default the WS2812 implementation uses the PIO0 peripheral


// wiring
#undef MATRIX_ROW_PINS
#undef MATRIX_COL_PINS
#undef MATRIX_ROW_PINS_RIGHT
#undef MATRIX_COL_PINS_RIGHT

#define MATRIX_ROW_PINS \
    { GP27, GP26, GP22, GP20 }
#define MATRIX_COL_PINS \
    { GP23, GP21, GP9, GP8, GP7, GP6, GP5, GP4 }
#define MATRIX_ROW_PINS_RIGHT \
    { GP4, GP5, GP6, GP7 }
#define MATRIX_COL_PINS_RIGHT \
    { GP8, GP9, GP21, GP23, GP20, GP22, GP26, GP27 }

#undef ENCODERS_PAD_A
#undef ENCODERS_PAD_B
#undef ENCODERS_PAD_A_RIGHT
#undef ENCODERS_PAD_B_RIGHT

#define ENCODERS_PAD_A \
    { GP29 }
#define ENCODERS_PAD_B \
    { GP28 }
#define ENCODERS_PAD_A_RIGHT \
    { GP28 }
#define ENCODERS_PAD_B_RIGHT \
    { GP29 }


#undef SPLIT_HAND_MATRIX_GRID
#define SPLIT_HAND_MATRIX_GRID GP7, GP20 // row first because the board is col2row
```

halconf.h
``` C
#pragma once

// #define PAL_USE_CALLBACKS   TRUE
#define HAL_USE_I2C         TRUE

#include_next <halconf.h>

```

mcuconf.h
``` C
#pragma once


#define RP2040_MCUCONF

#undef RP_I2C_USE_I2C0
#define RP_I2C_USE_I2C0 FALSE

#undef RP_I2C_USE_I2C1
#define RP_I2C_USE_I2C1 TRUE

#include_next <mcuconf.h>
```
