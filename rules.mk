# MCU name
# MCU = RP2040
# BOARD = GENERIC_RP_RP2040
# Bootloader selection
# BOOTLOADER = rp2040
# CONVERT_TO = kb2040

SERIAL_DRIVER = vendor

OLED_ENABLE = yes
OLED_DRIVER = ssd1306   # Enables the use of OLED displays

ENCODER_ENABLE = no     # Enables the use of one or more encoders

RGBLIGHT_ENABLE = yes   # Enable keyboard RGB underglow
RGB_MATRIX_ENABLE = no

TAP_DANCE_ENABLE = no

MOUSEKEY_ENABLE = yes
DEBOUNCE_TYPE = asym_eager_defer_pk
WPM_ENABLE = no

# DEFAULT_FOLDER = splitkb/kyria/rev2

# OPT_DEFS += -DRP2040_MCUCONF
# OPT_DEFS += -DRP_I2C_USE_I2C0=TRUE
# OPT_DEFS += -DRP_I2C_USE_I2C1=TRUE
# OPT_DEFS += -DHAL_USE_I2C=TRUE
# OPT_DEFS += -DPAL_USE_CALLBACKS

BOOTMAGIC_ENABLE = no      # Enable Bootmagic Lite

EXTRAKEY_ENABLE = yes      # Audio control and System control
CONSOLE_ENABLE = no        # Console for debug
COMMAND_ENABLE = no        # Commands for debug and configuration
NKRO_ENABLE = no           # Enable N-Key Rollover
BACKLIGHT_ENABLE = no      # Enable keyboard backlight functionality
AUDIO_ENABLE = no          # Audio output
