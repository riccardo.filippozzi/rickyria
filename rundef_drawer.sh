qmk c2json -kb splitkb/kyria/rev2 -km default ./keymaps/default/keymap.c -o rickyria.json --no-cpp
keymap -c ./config.yaml parse -q rickyria.json -c 10 --layer-names QWERTY NAV MOUSE SYM NUM FUNC ADJ CLMK CLMK-DH > rickyria.yaml
keymap -c ./config.yaml draw rickyria.yaml > rickyria.svg
