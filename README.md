

# RicKyria
(Riccardo + Kyria = RicKyria) == Genius (with a higher G, of course)

- [RicKyria](#rickyria)
  - [Keymap](#keymap)
  - [Additional Features](#additional-features)
    - [Fader](#fader)
      - [Configuration](#configuration)
        - [Time](#time)
        - [Saturation And Brigthness](#saturation-and-brigthness)
        - [Fade Transitions](#fade-transitions)
        - [Example: Fade At Startup](#example-fade-at-startup)

## Keymap
![Alt text](rickyria.png)

## Additional Features

### Fader
(I really don't really know how to call this feature)   
Fader allow you to do a smooth leds color and brigthness transition.  
#### Configuration
Call `fade_init()` at startup.
``` C
void keyboard_pre_init_user(){
    fade_init();
}
```
##### Time
To change transition time you can use `FD_TIME` (abbreviate FTIME) button. It sets the fade_time by taking the time from one of the values in the array `fade_time_options`.   
You can change default fade_time changing `fade_time_options` (array of time values) and `fade_time_option_index`(index of the array `fade_time_options`).   
> Note: if add or remove values from `fade_time_options` you have to change `FADE_TIME_OPTIONS_NUMBER` too.
``` C
#define FADE_TIME_OPTIONS_NUMBER 10
uint8_t fade_time_option_index = 2;
const uint16_t fade_time_options[] = { 0, 50, 100, 175, 250, 500, 750, 1000, 1500, 2500};
```
##### Saturation And Brigthness
`fade_default_hsv` store default saturation and brigthness.   
To change color saturation and brigthness you cand use `FD_SAT` (abbreviate FSAT) and `FD_VAL` (abbreviate FVAL).
``` C
HSV fade_default_hsv = {0, 255, 255};
```
##### Fade Transitions
Start a transition calling `fade_goto(R, G, B)`
``` C
fade_goto(RGB_RED);
fade_goto(255, 0, 0);
```
##### Example: Fade At Startup
At startup it leds will fade to white.
``` C
void keyboard_pre_init_user(){
    fade_init();
    fade_goto(RGB_WHITE);
}
```
